// scalastyle:off
logLevel := Level.Warn

resolvers ++= DefaultOptions.resolvers(snapshot = false)
resolvers ++= Seq(
  Resolver.bintrayIvyRepo("sbt", "sbt-plugin-releases"),
  Resolver.typesafeRepo("releases"),
  Resolver.sonatypeRepo("releases"),
  Resolver.DefaultMavenRepository,
  // Remove below resolver once the following issues has been resolved:
  // https://issues.jboss.org/projects/JBINTER/issues/JBINTER-21
  "JBoss" at "https://repository.jboss.org/"
)

// Dependency handling
addSbtPlugin("com.timushev.sbt" %% "sbt-updates" % "0.5.0")

// Style formatting
addSbtPlugin("org.scalameta"  %% "sbt-scalafmt"          % "2.2.0")
addSbtPlugin("org.scalastyle" %% "scalastyle-sbt-plugin" % "1.0.0")

// Test coverage
addSbtPlugin("org.scoverage" %% "sbt-scoverage" % "1.6.0")

// Packaging and release
addSbtPlugin("com.typesafe.sbt"  %% "sbt-native-packager" % "1.4.1")
addSbtPlugin("com.github.gseitz" %% "sbt-release"         % "1.0.12")
