package net.scalytica.kafka.topicadmin

import com.typesafe.scalalogging.LazyLogging
import net.scalytica.kafka.topicadmin.cli.{
  CliOptions,
  CliParser,
  ConsoleRenderer
}
import net.scalytica.kafka.topicadmin.client.KafkaAdminClient

import scala.concurrent.Await
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._

object AdminApp extends ConsoleRenderer with LazyLogging {

  val cfg = AppConfig.load()

  val argsParser = CliParser.parser(cfg)

  def main(args: Array[String]): Unit = {

    logger.debug(s"Application initialised with args: ${args.mkString(" ")}")

    argsParser.parse(args, CliOptions()).foreach { c =>
      implicit val adminClient: KafkaAdminClient = {
        val creds = for {
          u <- c.user
          p <- c.pass
        } yield (u, p)

        if (c.brokers.nonEmpty) {
          out(c.brokers)
          KafkaAdminClient(
            cfg = cfg.copy(kafkaUrl = c.brokers),
            credentials = creds
          )
        } else {
          KafkaAdminClient(cfg = cfg, credentials = creds)
        }
      }

      val output = Await.result(c.execute().recover {
        case t: Throwable =>
          err(s"Error: ${t.getMessage}")
          argsParser.usage
      }, 30 seconds)

      out(output)

      adminClient.close()
    }

  }

}
