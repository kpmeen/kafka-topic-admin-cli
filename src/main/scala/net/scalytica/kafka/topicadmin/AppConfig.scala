package net.scalytica.kafka.topicadmin

import com.typesafe.config.{Config, ConfigFactory}
import net.ceedubs.ficus.Ficus._
import net.ceedubs.ficus.readers.ArbitraryTypeReader._
import net.scalytica.kafka.topicadmin.models.Topic

case class AppConfig(
    clientId: String,
    kafkaUrl: String,
    clientProperties: Map[String, AnyRef] = Map.empty,
    topics: Seq[Topic] = Seq.empty
)

object AppConfig extends ExtraConfigReaders {

  def fromConfig(cfg: Config): AppConfig =
    cfg.getConfig("net.scalytica.kafka.admin").as[AppConfig]

  def load(): AppConfig = fromConfig(ConfigFactory.load())
}
