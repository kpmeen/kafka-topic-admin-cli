package net.scalytica.kafka.topicadmin.models

import org.apache.kafka.clients.admin.ConfigEntry

trait KafkaPropType {

  def toPropMap: Map[String, String]

  def configEntries: Seq[ConfigEntry] = toPropMap.toSeq.map {
    case (k, v) => new ConfigEntry(k, v)
  }

}
