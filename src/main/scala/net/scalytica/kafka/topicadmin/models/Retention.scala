package net.scalytica.kafka.topicadmin.models

import java.util.concurrent.TimeUnit

import org.apache.kafka.common.config.TopicConfig.{
  RETENTION_BYTES_CONFIG,
  RETENTION_MS_CONFIG
}

import scala.concurrent.duration.{Duration, FiniteDuration}

sealed trait Retention extends KafkaPropType

object Retention {

  // The next two variables will set the retention to permanent. None of these
  // values should be used if at least one of either retentionSize or
  // retentionDuration is set in the user provided config.
  val InfiniteRetentionBytes    = "-1"
  val InfiniteRetentionDuration = "-1"

  def isInfiniteArgs(s: Int, d: String): Boolean = {
    s.toString == InfiniteRetentionBytes && d == InfiniteRetentionDuration
  }

  def isInfiniteArgs(s: Int, d: Duration): Boolean = {
    s.toString == InfiniteRetentionBytes && !d.isFinite()
  }

  implicit def stringToDuration(d: String): Duration = {
    if (d == InfiniteRetentionDuration || d == "Inf") Duration.Inf
    else {
      val dur = Duration.apply(d)
      FiniteDuration(dur.toMillis, TimeUnit.MILLISECONDS)
    }
  }

  def from(
      maybeSize: Option[Int] = None,
      maybeDurationStr: Option[String] = None
  ): Retention = {
    (maybeSize, maybeDurationStr) match {
      // scalastyle:off line.size.limit
      case (Some(s), Some(d)) if isInfiniteArgs(s, d) => InfiniteRetention
      case (Some(s), Some(d))                         => TimeAndSizeRetention(d, s)
      case (Some(s), None)                            => SizeRetention(s)
      case (None, Some(d))                            => TimeRetention(d)
      case (None, None)                               => InfiniteRetention
      // scalastyle:on line.size.limit
    }
  }

  def apply(
      maybeSize: Option[Int] = None,
      maybeDuration: Option[Duration] = None
  ): Retention = {
    (maybeSize, maybeDuration) match {
      // scalastyle:off line.size.limit
      case (Some(s), Some(d)) if isInfiniteArgs(s, d) => InfiniteRetention
      case (Some(s), Some(d))                         => TimeAndSizeRetention(d, s)
      case (Some(s), None)                            => SizeRetention(s)
      case (None, Some(d))                            => TimeRetention(d)
      case (None, None)                               => InfiniteRetention
      // scalastyle:on line.size.limit
    }
  }

}

case object InfiniteRetention extends Retention {

  override def toPropMap = Map(
    RETENTION_BYTES_CONFIG -> Retention.InfiniteRetentionBytes,
    RETENTION_MS_CONFIG    -> Retention.InfiniteRetentionDuration
  )

}

case class TimeRetention(duration: Duration) extends Retention {
  override def toPropMap = {
    if (duration.isFinite()) {
      Map(RETENTION_MS_CONFIG -> duration.toMillis.toString)
    } else {
      Map(RETENTION_MS_CONFIG -> Retention.InfiniteRetentionDuration)
    }
  }
}

case class SizeRetention(size: Int) extends Retention {
  override def toPropMap = {
    val s = if (size < 0) Retention.InfiniteRetentionBytes else size.toString
    Map(RETENTION_BYTES_CONFIG -> s)
  }
}

case class TimeAndSizeRetention(duration: Duration, size: Int)
    extends Retention {
  override def toPropMap = {
    val dur =
      if (duration.isFinite()) Retention.InfiniteRetentionDuration
      else duration.toMillis.toString
    val sze =
      if (size.toString == Retention.InfiniteRetentionBytes)
        Retention.InfiniteRetentionBytes
      else size.toString
    Map(
      RETENTION_MS_CONFIG    -> dur,
      RETENTION_BYTES_CONFIG -> sze
    )
  }
}
