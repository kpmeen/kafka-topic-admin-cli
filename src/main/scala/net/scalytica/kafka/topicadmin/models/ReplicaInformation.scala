package net.scalytica.kafka.topicadmin.models

import org.apache.kafka.common.requests.DescribeLogDirsResponse.ReplicaInfo

case class ReplicaInformation(
    size: Long,
    offsetLag: Long,
    isFuture: Boolean
) {

  override def toString: String =
    s"ReplicaInformation{" +
      s"size: $size, " +
      s"offsetLag: $offsetLag, " +
      s"isFuture: $isFuture" +
      s"}"

}

object ReplicaInformation {

  implicit def fromKafkaModel(ri: ReplicaInfo): ReplicaInformation = {
    ReplicaInformation(
      size = ri.size,
      offsetLag = ri.offsetLag,
      isFuture = ri.isFuture
    )
  }

}
