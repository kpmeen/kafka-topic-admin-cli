package net.scalytica.kafka.topicadmin.cli

trait ConsoleRenderer {

  def out(str: String): Unit = println(str)

  def err(str: String): Unit = System.err.println(str)

}
