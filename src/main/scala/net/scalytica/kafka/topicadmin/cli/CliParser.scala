package net.scalytica.kafka.topicadmin.cli

import java.io.File

import net.scalytica.kafka.topicadmin._
import net.scalytica.kafka.topicadmin.AppConfig
import net.scalytica.kafka.topicadmin.cli.Commands._
import net.scalytica.kafka.topicadmin.models._
import scopt._

import scala.concurrent.duration.Duration

/**
 * Parser for CLI arguments passed to the application.
 *
 * @param cfg AppConfig to use for fallback configuration
 * @see https://github.com/scopt/scopt
 */
class CliParser(cfg: AppConfig)
    extends OptionParser[CliOptions]("kafka-topic-admin-cli") {

  note("\n")

  head("Kafka Topic Admin CLI")

  help("help").text("prints this usage text")

  note("\n")

  // Specify which kafka brokers to connect to. Falls back to value in config.
  // This argument is common for all commands.
  opt[String]('b', "kafka-brokers")
    .action((urlString, opts) => opts.copy(brokers = urlString))
    .text("Comma separated list of URLs to the kafka brokers.")
    .valueName("<host1:port>,<host2:port>,...")
    .required()
    .withFallback(() => cfg.kafkaUrl)

  note("\n")

  opt[Unit]('c', "cloud")
    .text("Provide credentials for Confluent Cloud")
    .children(
      opt[String]("token")
        .action((token, opts) => opts.copy(user = token.safeString))
        .valueName("<api-token>")
        .text("API token for Confluent Cloud.")
        .optional(),
      opt[String]("secret")
        .action((secret, opts) => opts.copy(pass = secret.safeString))
        .text("API secret for Confluent Cloud.")
        .valueName("<password> or <api-secret>")
        .optional()
    )

  note("\n")

  //---------------------------------------------------------------------------
  // COMMANDS
  //---------------------------------------------------------------------------

  cmd(ShowTopicNames.name)
    .action((_, opts) => opts.copy(command = Some(ShowTopicNames())))
    .text("List all non-internal topic names in the kafka cluster.")
    .children(
      opt[Unit]("include-internal")
        .abbr("i")
        .text("Include kafka internal topics")
        .optional()
        .action { (_, opts) =>
          opts.enrich { case s: ShowTopicNames => s.withIncludeInternalEnabled }
        }
    )

  note("\n")

  cmd(DescribeTopic.name)
    .action((_, opts) => opts.copy(command = Some(DescribeTopic())))
    .text("Show detailed topology information for a topic.")
    .children(
      arg[Seq[String]]("<topicName1>,<topicName2>,...")
        .text("A comma separated list of topic names.")
        .action { (topics, opts) =>
          opts.enrich { case show: DescribeTopic => show.withTopics(topics) }
        }
    )

  note("\n")

  cmd(DescribeLogDirs.name)
    .action((_, opts) => opts.copy(command = Some(DescribeLogDirs())))
    .text("Show detailed information about brokers")
    .children(
      arg[Seq[Int]]("<brokerId1>,<brokerId2>,...")
        .text("A comma separated list of broker ID's")
        .required()
        .action { (bids, opts) =>
          opts.enrich { case d: DescribeLogDirs => d.withBrokers(bids: _*) }
        },
      opt[String]("topic")
        .abbr("t")
        .text("Only show broker details where given topic is present.")
        .optional()
        .action { (topic, opts) =>
          opts.enrich { case d: DescribeLogDirs => d.withTopic(topic) }
        }
    )

  note("\n")

  cmd(CreateTopic.name)
    .action((_, opts) => opts.copy(command = Some(CreateTopic())))
    .text("Create a new topic in the kafka cluster")
    .children(
      arg[String]("<topicName>")
        .text("Name of the topic to create")
        .required()
        .action { (name, opts) =>
          opts.enrich { case c: CreateTopic => c.withName(name) }
        },
      opt[Int]("partitions")
        .abbr("p")
        .text("The total number of partitions to assign to the topic.")
        .validate { p =>
          if (p > 0) success
          else failure("Cannot create topic with 0 partitions")
        }
        .action { (p, opts) =>
          opts.enrich { case c: CreateTopic => c.withPartitions(p) }
        },
      opt[Int]("replication-factor")
        .abbr("rf")
        .text("On how many brokers should the topic be replicated.")
        .validate { rf =>
          if (rf > 0) success
          else failure("Topic must have a minimum replication factor of 1.")
        }
        .action { (rf, opts) =>
          opts.enrich { case c: CreateTopic => c.withReplicationFactor(rf) }
        },
      opt[String]("cleanup-policy").abbr("c").action { (policy, opts) =>
        opts.enrich {
          case c: CreateTopic =>
            val cp = CleanupPolicy.fromString(policy) match {
              case c: Compact =>
                c // FIXME: need to capture compact interval
              case p => p
            }
            c.withCleanupPolicy(cp)
        }
      },
      opt[Int]("retention-size")
        .abbr("rs")
        .text("Size limit of topic, in bytes, when records can be removed.")
        .action { (size, opts) =>
          opts.enrich {
            case c: CreateTopic =>
              c.retention match {
                case InfiniteRetention =>
                  c.withRetention(Retention(Some(size)))
                case tsr: TimeAndSizeRetention =>
                  c.withRetention(Retention(Some(size), Some(tsr.duration)))
                case TimeRetention(currDur) =>
                  c.withRetention(Retention(Some(size), Some(currDur)))
                case SizeRetention(_) =>
                  c.withRetention(Retention(Some(size)))
              }
          }
        },
      opt[Duration]("retention-duration")
        .abbr("rd")
        .text("How long records should be kept in the topic.")
        .action { (dur, opts) =>
          opts.enrich {
            case c: CreateTopic =>
              c.retention match {
                case InfiniteRetention =>
                  c.withRetention(Retention(maybeDuration = Some(dur)))
                case tsr: TimeAndSizeRetention =>
                  c.withRetention(Retention(Some(tsr.size), Some(dur)))
                case TimeRetention(_) =>
                  c.withRetention(Retention(maybeDuration = Some(dur)))
                case SizeRetention(currSize) =>
                  c.withRetention(Retention(Some(currSize), Some(dur)))
              }
          }
        }
    )

  note("\n")

  cmd(CreateOrUpdateTopics.name)
    .text("Create or update a batch of topics by parsing a string of configs")
    .children(
      opt[Seq[String]]("string")
        .abbr("s")
        .valueName("<configString>")
        .text("A properly formatted string containing the topic configs.")
        .action { (configs, opts) =>
          opts.copy(command = Some(CreateOrUpdateTopics.parse(configs)(this)))
        },
      opt[File]("file")
        .abbr("f")
        .valueName("<fileName>")
        .text("A file containing HOCON formatted topic configurations.")
        .action { (file, opts) =>
          opts.copy(command = Some(CreateOrUpdateTopics.fromFile(file)))
        }
    )

  note("\n")

  cmd(UpdateTopicPartitions.name)
    .text(
      "Update a topic by providing a new configuration, or increasing its " +
        "number of partitions."
    )
    .action((_, opts) => opts.copy(command = Some(UpdateTopicPartitions())))
    .children(
      arg[String]("<topicName>")
        .text("Name of the topic to update")
        .required()
        .action { (name, opts) =>
          opts.enrich {
            case u: UpdateTopicPartitions => u.withTopic(name)
          }
        },
      opt[Int]("partitions")
        .abbr("p")
        .text(
          "New total number of partitions to give the topic." +
            " NOTE: can only be incremented!"
        )
        .required() // TODO: Only required until custom config is supported
        .action { (num, opts) =>
          opts.enrich {
            case u: UpdateTopicPartitions => u.withPartitions(num)
          }
        }
    )

  cmd(DeleteTopic.name)
    .text("Delete a topic from the kafka cluster. ¡¡¡WARNING!!!")
    .children(
      arg[String]("<topicName>")
        .text("Name of the topic to delete")
        .required()
        .action { (name, opts) =>
          opts.copy(command = Some(DeleteTopic(name)))
        }
    )

  note("\n")
}

object CliParser {

  def parser(cfg: AppConfig): CliParser = new CliParser(cfg)

}
