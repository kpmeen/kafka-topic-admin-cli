package net.scalytica.kafka.topicadmin.generics

/**
 * This is a little Scala trick to get access to the companion object of a
 * generic type. It is quite useful whenever you need to access it, but don't
 * have any possibility to find it without a lot of boilerplate.
 *
 * @tparam T the type for which a companion object is needed.
 */
trait Companion[T] {
  type C

  def apply(): C
}

object Companion {

  /**
   * Implicit function that knows how to grab a hold of the companion object for
   * the provided type T. For this to work, any instance of T needs to have a
   * type class instance associated with it, that implements the [[Companion]]
   * trait defined above.
   *
   * @param comp type class to use for getting the companion of T
   * @tparam T the type for which to get the companion object
   * @return the companion object for the type T.
   */
  implicit def companion[T](implicit comp: Companion[T]): comp.C = comp()
}
