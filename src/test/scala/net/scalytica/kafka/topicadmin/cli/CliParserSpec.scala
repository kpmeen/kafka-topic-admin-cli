package net.scalytica.kafka.topicadmin.cli

import com.typesafe.config.ConfigFactory
import net.scalytica.kafka.topicadmin.AppConfig
import net.scalytica.kafka.topicadmin.cli.Commands._
import net.scalytica.kafka.topicadmin.models._
import org.scalatest.matchers.must.Matchers
import org.scalatest.wordspec.AnyWordSpec
import org.scalatest.{Assertion, OptionValues}

import scala.concurrent.duration._

// scalastyle:off magic.number
class CliParserSpec extends AnyWordSpec with Matchers with OptionValues {

  val testCfg = ConfigFactory.parseResources("application-test.conf").resolve()
  val cfg = AppConfig.fromConfig(testCfg)
  val parser = new TestCliParser(cfg)

  def argsMustFail(args: String*): Assertion = {
    parser.parse(args, CliOptions()) mustBe None
  }

  def parsedArgsMustBeCmd[T <: Cmd](
      args: Seq[String],
      exp: T,
      expBrokers: String = cfg.kafkaUrl
  ): Assertion = {
    val parsed = parser.parse(args, CliOptions()).value
    parsed.brokers mustBe expBrokers
    parsed.command.value mustBe exp
  }

  def parseHelp(args: Seq[String]): Assertion = {
    val parsed = parser.parse(args, CliOptions())
    parsed mustBe empty
  }

  "The cli parser" when {

    s"help argument is found" should {
      "print the help text to stdout" in {
        parseHelp(Seq("help"))
      }
    }

    s"parsing ${ShowTopicNames.name} commands" should {
      "return a command with default values" in {
        parsedArgsMustBeCmd(Seq(ShowTopicNames.name), ShowTopicNames())
      }
      "return a command with include internal topics enabled" in {
        parsedArgsMustBeCmd(
          Seq(ShowTopicNames.name, "--include-internal"),
          ShowTopicNames(true)
        )
      }
    }

    s"parsing ${DescribeTopic.name} commands" should {
      "return a describe command for topics" in {
        parsedArgsMustBeCmd(
          Seq(DescribeTopic.name, "foo,bar,baz"),
          DescribeTopic(topics = Seq("foo", "bar", "baz"))
        )
      }
      "fail when topics aren't provided as an argument" in {
        argsMustFail(DescribeTopic.name)
      }
    }

    s"parsing ${DescribeLogDirs.name} commands" should {
      "return a command listing broker log dirs" in {
        parsedArgsMustBeCmd(
          Seq(DescribeLogDirs.name, "0,1,2"),
          DescribeLogDirs(brokers = Seq(0, 1, 2))
        )
      }
      "return a command listing broker log dirs where topic exist" in {
        parsedArgsMustBeCmd(
          Seq(DescribeLogDirs.name, "0,1,2", "-t", "foo"),
          DescribeLogDirs(topic = Some("foo"), brokers = Seq(0, 1, 2))
        )
      }
      "fail when broker ids aren't provided" in {
        argsMustFail(DescribeLogDirs.name, "-t", "foo")
      }
    }

    s"parsing ${CreateTopic.name} commands" should {
      "return a command with only the name" in {
        parsedArgsMustBeCmd(
          Seq(CreateTopic.name, "test-topic"),
          CreateTopic(name = Some("test-topic"))
        )
      }
      "return a command with name and partitions" in {
        parsedArgsMustBeCmd(
          Seq(CreateTopic.name, "test-topic", "-p", "9"),
          CreateTopic(name = Some("test-topic"), partitions = 9)
        )
      }
      "return a command with name and replication factor" in {
        parsedArgsMustBeCmd(
          Seq(CreateTopic.name, "test-topic", "-rf", "2"),
          CreateTopic(name = Some("test-topic"), replFactor = 2)
        )
      }
      "return a command with name and cleanup policy" in {
        parsedArgsMustBeCmd(
          Seq(CreateTopic.name, "test-topic", "-c", "delete"),
          CreateTopic(name = Some("test-topic"), cleanupPolicy = Delete)
        )
      }
      "return a command with name and retention size" in {
        parsedArgsMustBeCmd(
          Seq(CreateTopic.name, "test-topic", "-rs", "222222"),
          CreateTopic(
            name = Some("test-topic"),
            retention = SizeRetention(222222)
          )
        )
      }
      "return a command with name and retention duration" in {
        parsedArgsMustBeCmd(
          Seq(CreateTopic.name, "test-topic", "-rd", "7days"),
          CreateTopic(
            name = Some("test-topic"),
            retention = TimeRetention(7 days)
          )
        )
      }
      "return a command with all properties set" in {
        val expBrokers = "foo:9092,bar:9092,baz:9092"
        parsedArgsMustBeCmd(
          Seq(
            // format: off
            CreateTopic.name,
            "test-topic",
            "--kafka-brokers", expBrokers,
            "-p", "9",
            "-rf", "2",
            "-c", "compact",
            "-rs", "222222",
            "-rd", "7days"
            // format: on
          ),
          CreateTopic(
            name = Some("test-topic"),
            partitions = 9,
            replFactor = 2,
            cleanupPolicy = Compact(),
            retention = TimeAndSizeRetention(7 days, 222222)
          ),
          expBrokers
        )
      }
      "fail when no topic name is provided" in {
        argsMustFail(CreateTopic.name, "-p", "9")
      }
    }

    s"parsing ${CreateOrUpdateTopics.name} commands" should {
      "return a command when given a string argument" in {
        val cfgArg = "{name=foo;partitions=5;replication-factor=2;" +
          "cleanup-policy=delete;retention-size=-1;retention-duration=Inf}," +
          "{name=bar;replication-factor=2}," +
          "{name=baz;retention-duration=7days}"

        parsedArgsMustBeCmd(
          Seq(CreateOrUpdateTopics.name, "--string", cfgArg),
          CreateOrUpdateTopics(
            Seq(
              CreateTopic(
                name = Some("foo"),
                partitions = 5,
                replFactor = 2,
                cleanupPolicy = Delete,
                retention = InfiniteRetention
              ),
              CreateTopic(
                name = Some("bar"),
                replFactor = 2
              ),
              CreateTopic(
                name = Some("baz"),
                retention = TimeRetention(7 days)
              )
            )
          )
        )
      }
      "return a command when given a file argument" in {
        val fname = "create-topics-test.conf"
        val filePath = getClass.getClassLoader.getResource(fname).getPath

        parsedArgsMustBeCmd(
          Seq(CreateOrUpdateTopics.name, "--file", filePath),
          CreateOrUpdateTopics(
            Seq(
              CreateTopic(
                name = Some("topic1"),
                cleanupPolicy = Compact()
              ),
              CreateTopic(
                name = Some("topic2"),
                partitions = 1,
                cleanupPolicy = Delete,
                retention = TimeRetention(7 days)
              ),
              CreateTopic(
                name = Some("topic3"),
                cleanupPolicy = Compact(Some(48 hours)),
                retention = SizeRetention(256)
              ),
              CreateTopic(
                name = Some("topic4"),
                partitions = 1,
                cleanupPolicy = Delete,
                retention = TimeAndSizeRetention(2 days, 256)
              )
            )
          )
        )
      }
    }

    s"parsing ${UpdateTopicPartitions.name} commands" should {
      "return a command with new number of partitions" in {
        parsedArgsMustBeCmd(
          Seq(UpdateTopicPartitions.name, "foo", "-p", "9"),
          UpdateTopicPartitions(topic = Some("foo"), numPartitions = Some(9))
        )
      }
      "fail when no topic name is provided with args present" in {
        argsMustFail(UpdateTopicPartitions.name, "-p", "9")
      }
      "fail when no topic name is provided" in {
        argsMustFail(UpdateTopicPartitions.name)
      }
      "fail when topic name is provided but no args" in {
        argsMustFail(UpdateTopicPartitions.name, "foo")
      }
    }

    s"parsing ${DeleteTopic.name} commands" should {
      "return a command with a topic name" in {
        parsedArgsMustBeCmd(Seq(DeleteTopic.name, "foo"), DeleteTopic("foo"))
      }
      "fail when no topic name is provided" in {
        argsMustFail(DeleteTopic.name)
      }
    }
  }

}

class TestCliParser(cfg: AppConfig) extends CliParser(cfg) {
  // Silence the console output
  override def showUsageOnError = false
  override def showTryHelp(): Unit = {}
  override def reportError(msg: String): Unit = {}
  override def reportWarning(msg: String): Unit = {}
}
