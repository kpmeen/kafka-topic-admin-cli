# Contributing

If you find this tool useful and you want to contribute, you are very welcome to
do so. Contributions can be anything from creating issues, writing
documentation or creating PR's.

## Guidelines

* All code PRs should come with:
  * a meaningful description
  * inline-comments for important things
  * unit tests (positive and negative)
  * a green build in CI
* Line length for the code base is set to a max of 80 characters. This is
  enforced by scalastyle.
* Before committing any code, please run scalafmt first to ensure the code is
  properly formatted.
* PR's that impact the correctness of the available documentation, should also
  come with an update to the documentation.
* Be prepared to discuss/argue-for your changes if you want them merged! You
  may need to refactor to ensure your changes fit into the larger codebase.
* Prefer functional/integration testing over unit testing if you have to choose.
  * It's OK if you feel the code doesn't need extra test coverage. But be
    prepared to argue why that's the case.
* It's entirely possible your changes won't be merged, or will get ripped out
  later. This is also the case for my changes, as the Author!
* Even a rejected/reverted PR is valuable! It helps to explore the solution
  space, and know what works not. Code is written and scrapped for better code
  all the time. 
* Feel free to send Proof-Of-Concept PRs that you don't intend to get merged.
  * If that's the case, add an appropriate label like "suggestion" or
  "discussion".
  
## Code of Conduct

Last but not least, try to remember that the software is created in free time
by a living, breathing human being. Free of charge. And from a CoC point of
view, please keep the following in mind:  

* Be polite and use common decency.
* This piece of software is provided as OSS, free of charge.
* The author is not a free developer resource at your disposal.
* The author is very likely to have other priorities than you.
  * Which is a perfect opportunity for _you_ to contribute with a PR or similar.